// S28 Activity Template:


// 1.) Insert a single room using the updateOne() method.
// Code here:

db.room.insertOne({
    "name": "single",
    "acommodates": 2,
    "price": 1000,
    "description": "a simple room with all the basic necessities",
    "rooms_available": 10,
    "isAvailable": false
})
// 2.) Insert multiple rooms using the insertMany() method.
// Code here:

db.room.insertMany([
        {
    "name": "double",
    "acommodates": 2,
    "price": 2000,
    "description": "a room fit for a small family goin on a vacation",
    "rooms_available": 5,
    "isAvailable": false
        },
        {
    "name": "double",
    "acommodates": 4,
    "price": 4000,
    "description": "a room fit with a queen sized bed perfect for a simple getaway",
    "rooms_available": 15,
    "isAvailable": false
        }
    ]);
// 3.) Use find() method to search for a room with a name "double".
// Code here:
db.room.find({
    "name": "double"
});

// 4.) Use the updateOne() method to update the queen room and set the available rooms to 0.
// Code here:

db.room.updateOne(
    {
    "name": "double",
    "acommodates": 4,
    "price": 4000,
    "description": "a room fit with a queen sized bed perfect for a simple getaway",
    "rooms_available": 15,
    "isAvailable": false
    },
    {
        $set: {
       "rooms_available": 0
        }
    })

// 5.) Use the deleteMany method to delete all rooms that have 0 rooms avaialable.
// Code here:

db.room.deleteMany({

   "rooms_available": 0
})